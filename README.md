# iriun-webcam

This Ansible role installs Iriun webcam on Ubuntu 18.04 or 22.04.

For Bionic I had to install `v4l2loopback` from source and set `exclusive_caps`
for `iriun` to work properly.

For Jammy, the approach used for Bionic did not work with iriun v2.4.1 (iriun
would start, but just show a black video feed and constantly reload).
After some limited testing, I think the entire approach used for Bionic is no longer
necessary on Jammy. Just install `v4l2loopback-dkms` and the latest available
`iriun-webcam` DEB file from https://iriun.com, and iriun seems to work fine.

I have not attempted to get microphone from the Android device (I only care
about the camera) for either Ubuntu version.


## Iriun version compatibility with Ubuntu 18.04

I was confused as to which version of the DEB-file to use on Ubuntu 18.04.

There appears to be two versions of the main Iriun website,
+ http://iriun.com (which points to iriunwebcam-2.4.1.deb *for Ubuntu 20.04 or later*), and
+ https://iriun.gitlab.io (which points to iriunwebcam-2.3.1.deb *for Ubuntu 18.04 or later*).

I have decided to stick with v2.3.1 for now. It appears to work well on Ubuntu 18.04.5.

## Refs

+ https://ask.fedoraproject.org/t/iriun-use-phone-as-a-webcam-only-deb-package/10344/4
+ https://askubuntu.com/questions/1263554/sudo-modprobe-v4l2loopback-modprobe-error-could-not-insert-v4l2loopback-bad
+ https://forums.linuxmint.com/viewtopic.php?t=319192
+ https://aur.archlinux.org/packages/iriunwebcam-bin/
+ https://askubuntu.com/questions/1232458/iriunwebcam-doesnt-work-on-20-04
+ https://github.com/umlaeute/v4l2loopback/issues/305
+ https://askubuntu.com/questions/1235389/droidcam-unable-to-find-dev-video0/1235432#1235432
