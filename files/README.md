# Important note regarding older DEB files

I have **manually edited** the `2.3.1`, `2.4`, and `2.4.1` DEB files and
**removed** their dependency on `v4l2loopback-dkms`.

These DEB files are only meant to be used when you have
compiled and built `v4l2loopback` from source
(which seems to be necessary on to make Iriun webcam work on Ubuntu Bionic).


Please note that the `2.7` file has not been altered in any way.

All DEB files were originally downloaded from [iriun.com](https://iriun.com).


## How to remove v4l2loopback dependency from DEB files

I have removed the `v4l2loopback` dependency by manually editing the DEB-file's
dependencies and then reconstructing the DEB file.
Because I will forget otherwise, here is how we did that.

Let's walk through the process here for the v2.4 DEB file.

```
taha@asks2:/media/bay/taha/projects/ansible/roles/public/iriun-webcam/files
$ mkdir unpacked-2.4
$ cp iriunwebcam-2.4.deb unpacked-2.4/
$ cd unpacked-2.4/
$ ar x iriunwebcam-2.4.db
$ rm iriunwebcam-2.4.deb
```

Unpacked, it looks like this:
```
$ ll
total 980K
drwxrwxr-x 2 taha taha 4.0K May 24 22:38 .
drwxrwxr-x 3 taha taha 4.0K May 24 22:34 ..
-rw-r--r-- 1 taha taha  584 May 24 22:37 control.tar.xz
-rw-r--r-- 1 taha taha 963K May 24 22:37 data.tar.xz
-rw-r--r-- 1 taha taha    4 May 24 22:37 debian-binary
```

Now unpack the control archive:
```
$ tar xf control.tar.xz
```
this created two files, `control` and `postinst`.
Now edit the `control` file using any text editor and remove the dependency `v4l2loopback-dkms`.

Repack `control.tar.xz` (uppercase `J` compresses using `xz`):
```
$ tar -cJvf control.tar.xz control postinst
```

Repack the DEB file (order is important):
```
$ ar rcs iriun-2.4.deb debian-binary control.tar.xz data.tar.xz
```

+ https://fixyacloud.wordpress.com/2020/01/26/how-do-i-get-apt-get-to-ignore-some-dependencies/
+ https://unix.stackexchange.com/questions/138188/easily-unpack-deb-edit-postinst-and-repack-deb
